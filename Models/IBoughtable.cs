﻿using UnityEngine;
using System.Collections;

public interface IBoughtable  {

	bool Buy();
	 bool IsBought {
		get;
		set;
	}
	int Price {
		get;
		set;
	}
}
