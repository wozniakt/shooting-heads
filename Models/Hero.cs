﻿using System;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Hero 
{

	[SerializeField]
	int id;
	public int Id {
		get
		{ return id; }
		set {
			id = value;
		}
	}

	[SerializeField]
	int hp;
	public int Hp {
		get
		{ return hp; }
		set {
			hp = value;
		}
	}

	[SerializeField]
	int maxHp;
	public int MaxHp {
		get
		{ return maxHp; }
		set {

			maxHp = value;

		}
	}

	[SerializeField]
	HeroState state;
	public HeroState State {
		get
		{ return state; }
		set {
			state = value;
		}
	}

	[SerializeField]
	string name;
	public string Name {
		get
		{ return name; }
		set {
			name = value;
		}
	}
	[SerializeField]
	float speedX_multipler;
	public float SpeedX_multipler{
		get{ return speedX_multipler;}
		set{ speedX_multipler = value;}
	}

	[SerializeField]
	float speedY_multipler;
	public float SpeedY_multipler{
		get{ return speedY_multipler;}
		set{ speedY_multipler = value;}
	}
}