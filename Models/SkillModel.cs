﻿using System;
using UnityEngine;
[System.Serializable]
	public class SkillModel:Item
	{
	[SerializeField]
	[CustomAttribite(name="Speed", customAttrType= CustomAttrType.Info)]
	public float Speed;

	[SerializeField]
	[CustomAttribite(name="Health", customAttrType= CustomAttrType.Info)]
	public float Health;


}

