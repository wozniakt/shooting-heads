﻿using System;
using UnityEngine;
[System.Serializable]
	public class Stat
	{
	[SerializeField]
	float max;
	public float Max {
		get{ return max; }
		set{ max = value; }
	}
//
//	[SerializeField]
//	float min;
//	public float Min {
//		get{ return min; }
//		set{ min = value; }
//	}
//
	public delegate	void StatChange(Stat stat);
	public event StatChange OnStatChange;

	[SerializeField]
	float current;
	public float Current {
		get{ return current; }
		set {
			if (value > Max) {
				value = Max;
			}

			current = value;
			if (OnStatChange != null) {
				OnStatChange (this);
			}
			if (current <= 0) {
				current = 0;
			}
		}
	}

	[SerializeField]
	float baseValue;
	public float BaseValue {
		get{ return baseValue; }
		set{ baseValue = value; }
	}


	[SerializeField]
	float multipler;
	public float Multipler {
		get{ return multipler; }
		set{ multipler = value; }
	}


	[SerializeField]
	string name;
	public string Name{
		get{ return name; }
		set{ name = value; }
	}

	public void Upgrade(){
		this.Max += this.Multipler;
		this.Current = this.Max;

	}

}

