﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class PoolManager : MonoBehaviour
{

	public static string POOLED_PREFAB_PATH = "Prefabs/";
	public static PoolManager instance;
	public GameObject HeroesPool, EffectsPool, BulletsPool, PowerUpsPool, EnemiesPool;
	public bool WillGrow = true;


	List<GameObject> pooledHeroes;
	List<GameObject> pooledEffects;
	List<GameObject> pooledBullets;
	List<GameObject> pooledPowerUps;
	List<GameObject> pooledEnemies;
	void Awake()
	{  
		instance = this;
		pooledEffects = new List<GameObject>();
		pooledBullets = new List<GameObject>();
		pooledEnemies = new List<GameObject>();
		pooledPowerUps = new List<GameObject>();

		createEffects(2,EffectType.Explosion1);
		createEffects(3,EffectType.Explosion2);

		createBullets (7, BulletType.Bullet1_enemy);
		createBullets (8, BulletType.Bullet1_hero);

		createEnemies (4, "Enemy1");
	}
		
	void createEffects(int count, EffectType effectType)
	{
		for (int i = 0; i < count; i++)
		{
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Prefabs_Effects/"+effectType.ToString()));
			obj.SetActive(false);
			obj.name = effectType.ToString();
			pooledEffects.Add(obj);
			obj.transform.SetParent(EffectsPool.transform);
		}

	}
	public GameObject GetPooledObject_Effect(EffectType effectType)
	{
		foreach (GameObject item in pooledEffects)
		{
			if (item.name == effectType.ToString())
			{     
				if (!item.activeInHierarchy)
				{
					return item;
				} else
				{
					
				}   
			}
		}

		if (WillGrow)
		{
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Prefabs_Effects/"+ effectType.ToString()));
			obj.name=effectType.ToString();
			pooledEffects.Add(obj);
			obj.transform.SetParent(EffectsPool.transform);
			return obj;
		}
		return null;
	}

	//************************Bullets
	void createBullets(int count, BulletType bulletType)
	{
		for (int i = 0; i < count; i++)
		{
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Prefabs_Bullets/"+bulletType.ToString()));
			obj.SetActive(false);
			obj.name = bulletType.ToString();
			pooledBullets.Add(obj);
			obj.transform.SetParent(BulletsPool.transform);
		}

	}

	//
	public GameObject GetPooledObject_Bullet(BulletType bulletType)
	{
		foreach (GameObject item in pooledBullets)
		{
			if (item.name == bulletType.ToString())
			{     
				if (!item.activeInHierarchy)
				{
					return item;
				} else
				{

				}   
			}
		}

		if (WillGrow)
		{
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Prefabs_Bullets/"+bulletType.ToString()));
			obj.name=bulletType.ToString();
			pooledBullets.Add(obj);
			obj.transform.SetParent(BulletsPool.transform);
			return obj;
		}
		return null;
	}

	//************************PowerUps
	void createPowerUps(int count, string powerUpName)
	{
		for (int i = 0; i < count; i++)
		{
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Prefabs_PowerUps/"+powerUpName.ToString()));
			obj.SetActive(false);
			obj.name = powerUpName.ToString();
			pooledPowerUps.Add(obj);
			obj.transform.SetParent(PowerUpsPool.transform);
		}

	}

	public GameObject GetPooledObject_PowerUp(string powerUpName)
	{
//		Debug.Log (POOLED_PREFAB_PATH +"Prefabs_PowerUps/"+powerUpName.ToString());
		foreach (GameObject item in pooledPowerUps)
		{
			if (item.name == powerUpName.ToString())
			{     
				if (!item.activeInHierarchy)
				{
					return item;
				} else
				{

				}   
			}
		}

		if (WillGrow)
		{
			//			///////Debug.Log (powerUpTypeNumber);
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Prefabs_PowerUps/"+powerUpName.ToString()));
			obj.name=powerUpName.ToString();
			pooledPowerUps.Add(obj);
			obj.transform.SetParent(PowerUpsPool.transform);
			return obj;
		}
		return null;
	}
	//************************Enemies
	void createEnemies(int count, string enemyName)
	{
		for (int i = 0; i < count; i++)
		{
			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Prefabs_Enemies/"+enemyName.ToString()));
			obj.SetActive(false);
			obj.name = enemyName.ToString();
// 			Debug.Log (enemyName);
			pooledEnemies.Add(obj);
			obj.transform.SetParent(EnemiesPool.transform);
		}

	}

	public GameObject GetPooledObject_Enemie(string enemyName)
	{
		foreach (GameObject item in pooledEnemies)
		{
			if (item.name == enemyName.ToString())
			{     
				if (!item.activeInHierarchy)
				{
					return item;
				} else
				{

				}   
			}
		}

		if (WillGrow)
		{

			GameObject obj = (GameObject)Instantiate(Resources.Load(POOLED_PREFAB_PATH +"Prefabs_Enemies/"+enemyName.ToString()));
			obj.name=enemyName.ToString();
			pooledEnemies.Add(obj);
			obj.transform.SetParent(EnemiesPool.transform);
			return obj;
		}
		return null;
	}

}







