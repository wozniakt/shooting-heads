﻿using System;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class PlayerData 
{

	[SerializeField]
	float comboCount;
	public float ComboCount
	{
		get
		{return comboCount;}
		set{

			comboCount= value;

		}
	}

	[SerializeField]
	int pointsCount;
	public int PointsCount
	{
		get
		{return pointsCount;}
		set{
			pointsCount= value;
		}
	}

	[SerializeField]
	int highscorePointsCount;
	public int HighscorePointsCount
	{
		get
		{return highscorePointsCount;}
		set{
			highscorePointsCount = Mathf.Max (highscorePointsCount, value);
		}
	}

	[SerializeField]
	int coinsCount;
	public int CoinsCount
	{
		get
		{return coinsCount;}
		set{
			if (value>=0) {
				coinsCount= value;
			} else {
				coinsCount = 0;		
			}	
			PlayerPrefs.SetInt ("CoinsCount", CoinsCount);
		}
	}

	[SerializeField]
	Hero currentHero;
	public Hero CurrentHero
	{
		get
		{return currentHero;}
		set{
			currentHero = value;}
	}

	[SerializeField]
	GameMode currentGameMode;
	public GameMode CurrentGameMode
	{
		get
		{return currentGameMode;}
		set{
			currentGameMode = value;}
	}

//	[SerializeField]
//	public List<Hero> HeroesList;
//	public List<Item> ItemsList;

}