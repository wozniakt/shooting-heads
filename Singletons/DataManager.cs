﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class DataManager : MonoBehaviour
{

	GlobalEventsManager globalEventsManager;
	public static DataManager instance;
	public List<SkillModel> SkillsList; 
	public List<GameMode> GameModesList;

	//heads list in UI
	public GameState oGameState;
	public PlayerData oPlayerData;
	public static string PREFAB_PATH = "Prefabs/";
	// Use this for initialization
	void Awake ()
	{
		if (instance == null) {
			instance = this;
		} else {
			Destroy (gameObject);
		}
		DontDestroyOnLoad (instance.gameObject);

		//SaveLoadData.Load ();
		//oPlayerData.CurrentHero = SaveLoadData.LoadedPlayerData.CurrentHero;
		if (oPlayerData==null) {
			Debug.Log ("new player data");
			oPlayerData = new PlayerData (); 	
		}
		//Debug.Log( SaveLoadData.LoadedPlayerData.PointsCount);
		//Debug.Log( oPlayerData.CurrentHero.Id);
	}

	void Start(){
		//SaveLoadData.Save (oPlayerData);
		globalEventsManager = GlobalEventsManager.instance;
		globalEventsManager.OnChangeGameState += this.ChangeGameState;
		globalEventsManager.OnGetPoints += this.UpdateData;

		globalEventsManager.OnChooseItemFromMenu_Heads += this.chooseNewCurrentHero;
		globalEventsManager.OnChooseItemFromMenu_GameModesint += this.chooseNewCurrentGameMode;
		globalEventsManager.TriggerGetPoints (oPlayerData.CoinsCount);

	}
	void OnDisable(){
		globalEventsManager.OnChangeGameState -= this.ChangeGameState;
		globalEventsManager.OnGetPoints -= this.UpdateData;
		globalEventsManager.OnChooseItemFromMenu_Heads -= this.chooseNewCurrentHero;
		globalEventsManager.OnChooseItemFromMenu_GameModesint -= this.chooseNewCurrentGameMode;
	}

	public void  ChangeGameState (GameState gameState)
	{
		oGameState = gameState;
		Time.timeScale = 1;
		switch (oGameState) {
		case GameState.Menu:
			break;
		case GameState.GameOn:
			break;
		case GameState.GameQuit:
			break;
		case GameState.GamePaused:
			//SaveLoadData.Save (oPlayerData);
			Time.timeScale = 0.0f;
			break;
		case GameState.GameResumed_AfterPause:
			break;
		case GameState.GameRestart_AfterPause:
			chooseNewCurrentHero (oPlayerData.CurrentHero.Id);
			break;
		case GameState.GameLost:
			Time.timeScale = 0.0f;
			break;
		case GameState.GameRestart_AfterLost:
			break;
		}
	}

	void UpdateData(int newPoints){
		oPlayerData.CoinsCount = newPoints + oPlayerData.CoinsCount;
	}

		
	public void chooseNewCurrentHero(int heroIndex){
	//	Debug.Log ("heroIndex "+heroIndex);
		Hero curHero = oPlayerData.CurrentHero;
		foreach (SkillModel oItem in SkillsList) {
//			Debug.Log ("oitem.id: " + oItem.Id + " Oitme.isbougth " + oItem.IsBought);
			if (oItem.IsBought && oItem.Id==heroIndex) {
				oPlayerData.CurrentHero.Id = oItem.Id;
				oPlayerData.CurrentHero.Hp = (int)oItem.Health;
			} else {
				oPlayerData.CurrentHero.Id = curHero.Id;
			}
		}
	}

	public void chooseNewCurrentGameMode(int gameModeIndex){

		GameMode curGameMode = oPlayerData.CurrentGameMode;
		foreach (GameMode oItem in GameModesList) {
			if (oItem.IsBought && oItem.Id==gameModeIndex) {
//				Debug.Log (oItem.Id + " oItem.Id");
				oPlayerData.CurrentGameMode.Id= oItem.Id;

			} else {
				oPlayerData.CurrentGameMode.Id = curGameMode.Id;
			}
		}
	}

}
