﻿using UnityEngine;
using System.Collections;


public enum GameState {Menu, GameOn, GamePaused,GameRestart_AfterPause,GameLost, GameRestart_AfterLost, GameResumed_AfterPause, GameQuit}
public enum SoundType{Click, Jump, Hit}

public enum BulletType{Bullet1_hero, Bullet1_enemy, Bullet_Axe_Hero, Bullet_Axe_Enemy,Bullet_Chess_Hero, Bullet_Chess_Enemy,
	Bullet_Sock_Hero, Bullet_Sock_Enemy,Bullet_Panties_Hero, Bullet_Panties_Enemy, Bullet_Orb_Hero, Bullet_Orb_Enemy}
public enum EffectType{Explosion1, Explosion2, ExplosionDeath1,ExplosionDeath2,ExplosionHit1, NuclearExplosion}


public enum HeroState{Falling, Jump, Dead, Idle,Grounded}

public enum HeadState{Shooting, Dead, Idle}

