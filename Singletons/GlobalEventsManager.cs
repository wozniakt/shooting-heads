﻿using UnityEngine;
using System.Collections;

public class GlobalEventsManager : MonoBehaviour
{

	public static GlobalEventsManager instance;
	void Awake ()
	{
		if (instance == null)
			instance = this;
		else if (instance != this) {
			Destroy (gameObject);
			return;
		}
		DontDestroyOnLoad (instance.gameObject);
	}


	public delegate void ChangeHeadState(HeadState oHeadState);
	public event ChangeHeadState OnChangeHeadState;

	public void TriggerChangeHeadState(HeadState oHeadState){
		if (OnChangeHeadState!=null) {
			OnChangeHeadState (oHeadState); 
		}
	}


	public delegate void ChangeHeroState(HeroState oHeroState);
	public event ChangeHeroState OnChangeHeroState;

	public void TriggerChangeHeroStateo(HeroState oHeroState){
		if (OnChangeHeroState!=null) {
			OnChangeHeroState (oHeroState); 
		}
	}

	public delegate void UpdateData();
	public event UpdateData eUpdateData;

	public void TriggerUpdateData(){
		if (eUpdateData!=null) {
			eUpdateData (); 
		}
	}

	public delegate void ChangeGameState(GameState gameState);
	public event ChangeGameState OnChangeGameState;

	public void TriggerOnChangeGameState (GameState gameState)
	{
		if (OnChangeGameState != null) {
			OnChangeGameState (gameState); 
		}
	}

	public delegate void GetPoints(int newPoints);
	public event GetPoints OnGetPoints;

	public void TriggerGetPoints (int newPoints)
	{
		if (OnGetPoints != null) {
			OnGetPoints (newPoints); 
		}
	}	
	public delegate void GetHit(float newHpAmount);
	public event GetHit OnGetHit;

	public void TriggerGetHit (float newHpAmount)
	{
		if (OnGetHit != null) {
			OnGetHit (newHpAmount); 
		}
	}

	public delegate void ShootAmmo(float newAmmoAmount);
	public event ShootAmmo OnShootAmmo;

	public void TriggerOnShootAmmo (float newAmmoAmount)
	{
		if (OnShootAmmo != null) {
			OnShootAmmo (newAmmoAmount); 
		}
	}

	public delegate void ChooseItemFromMenu_Heads(int itemIndex);  // do wybrania elementu ze scrollbara
	public event ChooseItemFromMenu_Heads OnChooseItemFromMenu_Heads;

	public void TriggerChooseItemFromMenu_Heads (int itemIndex)
	{
		if (OnChooseItemFromMenu_Heads != null) {
			OnChooseItemFromMenu_Heads (itemIndex); 
		}
	}

	public delegate void ChooseItemFromMenu_GameModes(int itemIndex);  // do wybrania elementu ze scrollbara
	public event ChooseItemFromMenu_GameModes OnChooseItemFromMenu_GameModesint;

	public void TriggerChooseItemFromMenu_GameModes (int itemIndex)
	{
		if (OnChooseItemFromMenu_GameModesint != null) {
			OnChooseItemFromMenu_GameModesint (itemIndex); 
		}
	}

}
