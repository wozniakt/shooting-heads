﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class UI_Manager : MonoBehaviour
{
	GlobalEventsManager globalEventsManager;

	public Button Button_Menu_Start, Button_Menu_Quit;
	public Button  Button_Pause,Button_Pause_Restart, Button_Pause_BackToMenu, Button_Pause_Resume;
	public Button  Button_GameOver_Restart, Button_GameOver_Quit, Button_GameOver_GoToMenu;

	public GameObject Panel_Hud, Panel_Gameplay;
	public GameObject Panel_GameOver, Panel_Pause, Panel_Menu;

	public static string PREFAB_PATH = "Prefabs/";
	public Text pointsText;
	public Image hpImage, ammoImage;

	public static UI_Manager instance;
	public GameState oGameState;

	GameObject gamePlayPanel;

	Animator Img_RandomEvent_Animator,PanelBackground_Animator;
	void Awake ()
	{
		if (instance == null)
			instance = this;
		else if (instance != this) {
			Destroy (gameObject);
			return;
		}	
		DontDestroyOnLoad (instance.gameObject);
	}

	void Start(){
		AddListenersToButtons ();
		globalEventsManager = GlobalEventsManager.instance;
		globalEventsManager.OnChangeGameState += this.ChangeGameStateUI;
		globalEventsManager.OnGetPoints += this.UpdateHud_Points;
		globalEventsManager.OnGetHit += this.UpdateHud_Hp;
		globalEventsManager.OnShootAmmo += this.UpdateHud_Ammo;
		globalEventsManager.TriggerOnChangeGameState (GameState.Menu);

	}
	void OnDisable(){
		globalEventsManager.OnChangeGameState -= this.ChangeGameStateUI;
		globalEventsManager.OnGetPoints -= this.UpdateHud_Points;
		globalEventsManager.OnGetHit -= this.UpdateHud_Hp;
		globalEventsManager.OnShootAmmo -= this.UpdateHud_Ammo;
	}

	public void  ChangeGameStateUI (GameState gameState){
		StartCoroutine (ChangeGameStateUI_Coroutine (gameState));
	}

	public IEnumerator  ChangeGameStateUI_Coroutine (GameState gameState)
	{ 
		Panel_Menu.SetActive (false); 
		//Panel_Gameplay.SetActive (false); 
		Panel_Pause.SetActive (false);  
		Panel_GameOver.SetActive (false); 
		Panel_Hud.SetActive (false);
		oGameState = gameState;
		switch (oGameState) {
		case GameState.Menu:
			Panel_Menu.SetActive (true); 
			GameplayContainerActive (false);
			break;
		case GameState.GameOn:
//			SaveLoadData.Save (DataManager.instance.oPlayerData);

			Panel_Menu.SetActive (false); 
			GameplayContainerActive (true);
			Panel_Hud.SetActive (true);
			LoadHeroOnScene (DataManager.instance.oPlayerData.CurrentHero.Id);
			UpdateHud_Hp (DataManager.instance.oPlayerData.CurrentHero.Hp);
			UpdateHud_Ammo (PlayerSingleton.instance.GetComponent<AmmoClip> ().BulletsCount);
			UpdateHud_Points (DataManager.instance.oPlayerData.CoinsCount);
			//LoadHeroOnScene (DataManager.instance.oPlayerData.CurrentHero.Id);
			break;
		case GameState.GameQuit:
			Application.Quit();
			break;
		case GameState.GamePaused:
			Panel_Pause.SetActive (true);
			break;
		case GameState.GameResumed_AfterPause:
			Panel_Pause.SetActive (false); 
			Panel_Hud.SetActive (true);
			break;
		case GameState.GameRestart_AfterPause:
			Panel_Pause.SetActive (false); 
			Panel_Hud.SetActive (true);
			GameplayContainerActive (false);
			yield return new WaitForSeconds (0.00f);
			GameplayContainerActive (true);
			LoadHeroOnScene (DataManager.instance.oPlayerData.CurrentHero.Id);
			UpdateHud_Hp (DataManager.instance.oPlayerData.CurrentHero.Hp);
			UpdateHud_Ammo (PlayerSingleton.instance.GetComponent<AmmoClip> ().BulletsCount);
			UpdateHud_Points (DataManager.instance.oPlayerData.CoinsCount);
			break;
		case GameState.GameLost:
			Panel_GameOver.SetActive (true);
			break;
		case GameState.GameRestart_AfterLost:
			Panel_GameOver.SetActive (false);
			GameplayContainerActive (false);
			yield return new WaitForSeconds (0.01f);
			GameplayContainerActive (true);
			Panel_Hud.SetActive (true);
			//

			LoadHeroOnScene (DataManager.instance.oPlayerData.CurrentHero.Id);
			UpdateHud_Hp (DataManager.instance.oPlayerData.CurrentHero.Hp);
			UpdateHud_Ammo (PlayerSingleton.instance.GetComponent<AmmoClip> ().BulletsCount);
			UpdateHud_Points (DataManager.instance.oPlayerData.CoinsCount);
			break;
		}
	}

	void AddListenersToButtons(){
		Button_Menu_Start.onClick.RemoveAllListeners ();
		Button_Menu_Start.onClick.AddListener (() => globalEventsManager.TriggerOnChangeGameState (GameState.GameOn));
		Button_Menu_Quit.onClick.RemoveAllListeners ();
		Button_Menu_Quit.onClick.AddListener (() => globalEventsManager.TriggerOnChangeGameState (GameState.GameQuit));

		Button_Pause.onClick.RemoveAllListeners ();
		Button_Pause.onClick.AddListener (() => globalEventsManager.TriggerOnChangeGameState (GameState.GamePaused));
		Button_Pause_BackToMenu.onClick.RemoveAllListeners ();
		Button_Pause_BackToMenu.onClick.AddListener (() => globalEventsManager.TriggerOnChangeGameState (GameState.Menu));
		Button_Pause_Resume.onClick.RemoveAllListeners ();
		Button_Pause_Resume.onClick.AddListener (() => globalEventsManager.TriggerOnChangeGameState (GameState.GameResumed_AfterPause));
		Button_Pause_Restart.onClick.RemoveAllListeners ();
		Button_Pause_Restart.onClick.AddListener (() => globalEventsManager.TriggerOnChangeGameState (GameState.GameRestart_AfterPause));

		Button_GameOver_GoToMenu.onClick.RemoveAllListeners ();
		Button_GameOver_GoToMenu.onClick.AddListener (() => globalEventsManager.TriggerOnChangeGameState (GameState.Menu));
		Button_GameOver_Quit.onClick.RemoveAllListeners ();
		Button_GameOver_Quit.onClick.AddListener (() => globalEventsManager.TriggerOnChangeGameState (GameState.GameQuit));
		Button_GameOver_Restart.onClick.RemoveAllListeners ();
		Button_GameOver_Restart.onClick.AddListener (() => globalEventsManager.TriggerOnChangeGameState (GameState.GameRestart_AfterLost));
	}

	void UpdateHud_Hp(float newHpAmount){
//		Debug.Log ("update hud");
		hpImage.fillAmount = newHpAmount/10;
	}

	void UpdateHud_Points(int newPoints){
		pointsText.text = DataManager.instance.oPlayerData.CoinsCount.ToString ();
	}

	void UpdateHud_Ammo(float newAmmoAmount){
		ammoImage.fillAmount = newAmmoAmount/10;
	}

	void GameplayContainerActive(bool shouldBeActive){
		if (shouldBeActive) {
			gamePlayPanel =Instantiate(Resources.Load<GameObject> ("Prefabs/Prefabs_Gameplays/GamePlayContainer"+DataManager.instance.oPlayerData.CurrentGameMode.Id));

		}
		if (!shouldBeActive) {
			Destroy(gamePlayPanel);
		}
	}

	public void LoadHeroOnScene(int heroId){

		GameObject hero = (GameObject)Instantiate(Resources.Load(PREFAB_PATH +"Prefabs_Heroes/Head"+DataManager.instance.oPlayerData.CurrentHero.Id));
		hero.transform.SetParent ( GameObject.Find ("HeroPlaceHolder").transform);
		hero.transform.position= GameObject.Find ("HeroPlaceHolder").transform.position;
		DataManager.instance.oPlayerData.CurrentHero.Hp = hero.transform.GetComponent<HP> ().HealthPoints;
		hero.SetActive (true);
		hero.GetComponent<SpriteRenderer> ().enabled= (true);
	
	}
}
