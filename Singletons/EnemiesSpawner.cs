﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

public class EnemiesSpawner : MonoBehaviour
{
	public List<GameObject> EnemiesList;
	public float intervalSingleEnemy, intervalGroupEnemies;
	public int enemiesInGroup;
	GameObject enemy;
	public bool isStartingPoint;//musi być zaznaczony jeśli mamy używać poniższej zmiennej:
	public GameObject startingPoint;
	public bool startAtGroup;
	public int horizontalInt, verticalInt;
	void Start ()
	{
		StartCoroutine (SpawnEnemy ( intervalSingleEnemy,intervalGroupEnemies));
	}
	
	// Update is called once per frame
	IEnumerator SpawnEnemy (float intervalSingleEnemy,float intervalGroupEnemies)
	{
		for (int i = 0; i < enemiesInGroup; i++) {
			
			enemy = PoolManager.instance.GetPooledObject_Enemie ((EnemiesList[Random.Range(0,EnemiesList.Count)]).name.ToString());

			if (isStartingPoint==true) {
				if (startAtGroup) {
					enemy.transform.position =  new Vector3( startingPoint.transform.position.x+(i*horizontalInt),
						this.transform.position.y+(i*verticalInt),90);
				} else {

				enemy.transform.position =  new Vector3( startingPoint.transform.position.x,
					this.transform.position.y,90);
				}
			} else {
				enemy.transform.position = new Vector3( this.transform.position.x+Random.Range(-3,3),
					this.transform.position.y+Random.Range(-3,3),90);
			}
				enemy.SetActive (true);
				yield return new WaitForSeconds (intervalSingleEnemy);
			}
		yield return new WaitForSeconds (intervalGroupEnemies);
		StartCoroutine (SpawnEnemy (intervalSingleEnemy,intervalGroupEnemies));
	}
}

