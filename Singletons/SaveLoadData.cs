﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

[System.Serializable]
public static class SaveLoadData 
{

	public static PlayerData LoadedPlayerData=null;



	public static void Save(PlayerData oPlayerData) {

		BinaryFormatter bf = new BinaryFormatter();
		FileStream file = File.Create (Application.persistentDataPath + "/savedGames.gd"); //you can call it anything you want
		bf.Serialize(file, oPlayerData);
		file.Close();
	}	

	public static void Load() {

		if (File.Exists (Application.persistentDataPath + "/savedGames.gd")) {
			BinaryFormatter bf = new BinaryFormatter ();
			//			///////Debug.Log ("Load saveloaddatda" + Application.persistentDataPath);
			FileStream file = File.Open (Application.persistentDataPath + "/savedGames.gd", FileMode.Open);
			LoadedPlayerData= (PlayerData)bf.Deserialize (file);

			file.Close ();
			//		///////Debug.Log ("LoadedPlayerData : " + LoadedPlayerData.CoinsCount);
		}

	}

	public static void ClearData() {
		if (File.Exists (Application.persistentDataPath + "/savedGames.gd")) {
			BinaryFormatter bf = new BinaryFormatter ();
			File.Delete (Application.persistentDataPath + "/savedGames.gd");
			//file.Close ();
		}}


	// Use this for initialization


	// Update is called once per frame




}

