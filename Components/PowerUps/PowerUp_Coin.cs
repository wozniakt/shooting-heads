﻿using UnityEngine;
using System.Collections;

public class PowerUp_Coin : MonoBehaviour
{
	public int coinsValue;
	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void OnTriggerEnter2D (Collider2D col)
	{
		
		if (col.CompareTag("Hero")) {

			GlobalEventsManager.instance.TriggerGetPoints (coinsValue);
			this.gameObject.SetActive (false);

		}
	}
}

