﻿using UnityEngine;
using System.Collections;

public class PowerUp_Bolt : MonoBehaviour
{
	Animator animator;
	GameObject explosion;
	// Use this for initialization
	void OnEnable ()
	{
		//animator = GetComponent<Animator> ();
		//animator.enabled = true;
	}

	// Update is called once per frame
	void OnTriggerEnter2D (Collider2D col)
	{

		if (col.CompareTag("Hero")) {
			//animator.enabled = false;
			explosion=PoolManager.instance.GetPooledObject_Effect (EffectType.NuclearExplosion);
			explosion.transform.position = this.transform.position;
			explosion.SetActive (true);
			this.gameObject.SetActive (false);
		}
	}
}
