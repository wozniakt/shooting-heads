﻿using UnityEngine;
using System.Collections;

public class PowerUps_Spawner : MonoBehaviour
{
		public float interval;
		GameObject powerUp;
		public int MinPowerUpLevel, MaxPowerUpLevel ;

		// Use this for initialization
		void Start ()
		{
			StartCoroutine (SpawnPowerUp ( interval));
		}

		// Update is called once per frame
	IEnumerator SpawnPowerUp (float interval)
		{
		yield return new WaitForSeconds (interval/2);
			//Debug.Log (Random.Range (MinPowerUpLevel, MaxPowerUpLevel + 1));
			powerUp = PoolManager.instance.GetPooledObject_PowerUp ("PowerUp"+Random.Range(MinPowerUpLevel, MaxPowerUpLevel+1));
			powerUp.transform.position = new Vector3( this.transform.position.x+Random.Range(-3,3),
													  this.transform.position.y+Random.Range(-3,3),90);
			powerUp.SetActive (true);
			yield return new WaitForSeconds (interval/2);
		StartCoroutine (SpawnPowerUp ( interval));
		}
}

