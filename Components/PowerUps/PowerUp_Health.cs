﻿using UnityEngine;
using System.Collections;

public class PowerUp_Health : MonoBehaviour
{
	public int hpValue;
	// Use this for initialization
	void Start ()
	{

	}

	// Update is called once per frame
	void OnTriggerEnter2D (Collider2D col)
	{

		if (col.CompareTag("Hero")) {
			PlayerSingleton.instance.GetComponent<HP> ().HealthPoints = PlayerSingleton.instance.GetComponent<HP> ().HealthPoints + hpValue;
			GlobalEventsManager.instance.TriggerGetHit (PlayerSingleton.instance.GetComponent<HP>().HealthPoints);
			this.gameObject.SetActive (false);

		}
	}
}

