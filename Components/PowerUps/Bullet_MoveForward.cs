﻿using UnityEngine;
using System.Collections;

public class Bullet_MoveForward : MonoBehaviour
{
	public Vector2 targetDirection;

	Rigidbody2D rigidBullet2D;
	Vector2 direction;
	public bool isRotating;
	public float rotateSpeed, movingSpeed;
	void Start ()
	{
		rigidBullet2D = GetComponent<Rigidbody2D> ();

	}

	void Update ()
	{

		direction = (new Vector2(this.targetDirection.x, this.targetDirection.y))-(Vector2)this.transform.position;
		direction.Normalize();
		float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
		if (isRotating) {
			transform.Rotate (0,0,rotateSpeed*Time.deltaTime); //rotates 50 degrees per second around z axis
		} else {
			this.transform.rotation = Quaternion.Euler(0f, 0f, angle - 90);
		}

		rigidBullet2D.velocity = direction * movingSpeed; //bulletspeed

	}
}

