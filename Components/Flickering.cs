﻿using UnityEngine;
using System.Collections;
//AND INVISIBLITY!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
public class Flickering : MonoBehaviour //AND INVISIBLITY!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
{//AND INVISIBLITY!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	[HideInInspector]
	public Renderer renderer;
	public float DelayTime, IntervalTime;
	public int flickCounter;
	int baseLayer;
	public bool isInvisible;
	// Use this for initialization

	void Start(){
		baseLayer=this.gameObject.layer;
	}

	public void StartFlickering ()
	{
		
		//renderer =new Renderer();
		this.gameObject.layer = 13;
		renderer = GetComponent<Renderer> ();
		StartCoroutine (flickering (DelayTime, IntervalTime));
	}

	// Update is called once per frame
	IEnumerator  flickering (float DelayTime, float IntervalTime)
	{
		for (int i = 0; i < flickCounter; i++) {
			
	
		yield return new WaitForSeconds(DelayTime);
		for (float f = 1f; f >= 0; f -= 0.1f) {

			Color c = renderer.material.color;
			c.a = f;
			renderer.material.color = c;
			yield return null;
		}
		//Debug.Log ("Fade out");
		yield return new WaitForSeconds(IntervalTime);
		for (float f = 0f; f <= 1; f += 0.1f) {

			Color c = renderer.material.color;
			c.a = f;
			renderer.material.color = c;
			yield return null;
		}
		//Debug.Log ("Fade in");
		yield return new WaitForSeconds(IntervalTime);
		//StartCoroutine (flickering (0, IntervalTime));
		}

		this.gameObject.layer = baseLayer;
	}
	void OnDisable(){
		if (renderer) {
			Color c = renderer.material.color;
			c.a = 1;
			renderer.material.color = c;
		}

	}
}

