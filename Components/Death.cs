﻿using UnityEngine;
using System.Collections;

public class Death : MonoBehaviour
{
	GameObject explosion;
	public EffectType effect;

	

	public IEnumerator _Death ()
	{
		

			explosion=PoolManager.instance.GetPooledObject_Effect (effect);
			explosion.transform.position = this.transform.position;
			explosion.SetActive (true);

		if (this.gameObject.CompareTag("Hero")) {
			this.GetComponent<SpriteRenderer> ().enabled= (false);
			yield return new WaitForSeconds (1f);

			GlobalEventsManager.instance.TriggerOnChangeGameState (GameState.GameLost);

		}
		this.gameObject.SetActive (false);
	}
}

