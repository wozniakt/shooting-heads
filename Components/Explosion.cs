﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour
{
	GameObject explosion;
	public EffectType effect;
	void OnTriggerEnter2D (Collider2D other)
	{
		explosion=PoolManager.instance.GetPooledObject_Effect (effect);
		explosion.transform.position = this.transform.position;
		explosion.SetActive (true);
	}

}

