﻿using UnityEngine;
using System.Collections;

public class DestroyTaggedObject : MonoBehaviour
{

	public string tag;
	
	// Update is called once per frame
	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.CompareTag (tag)) {
			other.gameObject.SetActive (false);
	
		}
	}
}

