﻿using UnityEngine;
using System.Collections;

public class EnableAfterTime : MonoBehaviour
{

	public float timeToEnable;
	public GameObject objectToEnable;
	void OnEnable ()
	{
		//objectToEnable = GameObject.Find ("Enemy");
		StartCoroutine (Enable (timeToEnable));
	}

	IEnumerator Enable (float timeToEnable)
	{
		yield return new WaitForSeconds (timeToEnable);
		objectToEnable.SetActive (true);
	}
}

