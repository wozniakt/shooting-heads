﻿using UnityEngine;
using System.Collections;

public class BulletStuff : MonoBehaviour
{
	public Vector2 targetDirection;
	public float angle ;
	Rigidbody2D rigidBullet2D;
	public Vector2 direction;
	public bool isRotating, isShooting;
	public float rotateSpeed, movingSpeed;
	void Start ()
	{
		rigidBullet2D = GetComponent<Rigidbody2D> ();

	}

	void OnEnable ()
	{
		

	}

	void Update ()
	{
			if (isRotating) {
				transform.Rotate (0,0,rotateSpeed*Time.deltaTime); //rotates 50 degrees per second around z axis
			} else {
				this.transform.rotation = Quaternion.Euler(0f, 0f, angle - 90);
			}
			rigidBullet2D.velocity = direction * movingSpeed; //bulletspeed

	}
}

