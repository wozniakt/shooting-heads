﻿using UnityEngine;
using System.Collections;

public class AmmoClip : MonoBehaviour
{
	public float BulletsCount;
	public float BulletsCountMax;
	public float reloadTimeFor1Bullet;
	public bool isReloading=false;
	public float fillAmountImageAmmo;
	public float reloadRatio;
	// Use this for initialization


	public void ReloadAmmoClip(){
		StartCoroutine (Reload ());
	}
	IEnumerator Reload(){
		isReloading = true;
//		Debug.Log (reloadTimeFor1Bullet * BulletsCountMax);
		for ( fillAmountImageAmmo = 0; fillAmountImageAmmo < BulletsCountMax/10; fillAmountImageAmmo=BulletsCount/10) {
//			Debug.Log ("isReloading " + fillAmountImageAmmo);
			yield return new WaitForSeconds (reloadTimeFor1Bullet);
			BulletsCount++;
		}

		//BulletsCount = BulletsCountMax;
		GlobalEventsManager.instance.TriggerOnShootAmmo (BulletsCount);
		isReloading = false;
	}
	void Update(){
		if (isReloading==true) {
			UI_Manager.instance.ammoImage.fillAmount = fillAmountImageAmmo ;
		}
	}
}

