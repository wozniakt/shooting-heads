﻿using UnityEngine;
using System.Collections;
using System;

public class DisableAfterTime : MonoBehaviour
{
	public float timeToDisable;
	void OnEnable ()
	{
		StartCoroutine (disable ());
	}

	IEnumerator disable ()
	{
		yield return new WaitForSeconds (timeToDisable);
		this.gameObject.SetActive (false);
	}
}
