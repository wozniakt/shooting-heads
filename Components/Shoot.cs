﻿using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour
{

	GameObject bulletGO;
	Rigidbody2D bulletRigidBody2D;
	Transform crosshair;
	public BulletType bulletType;
	public float roundsCount;
	public float bulletsInRound;
	public float intervalBulletsInRound;
	public float intervalRounds;
	AmmoClip ammoClip;

	void Start(){
		crosshair = transform.Find ("Crosshair");
		ammoClip = GetComponent<AmmoClip> ();
	}

	public void startShooting(BulletType bulletType, float roundsCount, float bulletsInRound, float intervalBulletsInRound,
		float intervalRounds){
		StartCoroutine(Shot(bulletType,roundsCount,bulletsInRound ,intervalBulletsInRound,intervalRounds));
	}

	IEnumerator Shot(BulletType bulletType, float roundsCount, float bulletsInRound, float intervalBulletsInRound,
		float intervalRounds){
		for (int i = 0; i < roundsCount; i++) {
			yield return new WaitForSeconds (intervalRounds);
			for (int j = 0; j < bulletsInRound; j++) {
				bulletGO=PoolManager.instance.GetPooledObject_Bullet (bulletType);
				bulletRigidBody2D = bulletGO.GetComponent<Rigidbody2D> ();
				bulletGO.transform.position = new Vector2 (this.transform.position.x, this.transform.position.y);
//				bulletGO.GetComponent<BulletStuff> ().targetDirection = new Vector2 (crosshair.position.x+(bulletsInRound/2-j)*2,crosshair.position.y+(bulletsInRound/2-j)*2);
				bulletGO.GetComponent<BulletStuff> ().targetDirection = new Vector2 (crosshair.position.x,crosshair.position.y);
				bulletGO.GetComponent<BulletStuff> ().direction =( (new Vector2(bulletGO.GetComponent<BulletStuff> ().targetDirection.x, bulletGO.GetComponent<BulletStuff> ().targetDirection.y))-(Vector2)bulletGO.transform.position);

				bulletGO.GetComponent<BulletStuff> ().direction.Normalize();

				bulletGO.GetComponent<BulletStuff> ().angle = Mathf.Atan2(bulletGO.GetComponent<BulletStuff> ().direction.y, bulletGO.GetComponent<BulletStuff> ().direction.x) * Mathf.Rad2Deg; //skierownie pocisku pod odpowiednim kątem
				bulletGO.SetActive (true);
				yield return new WaitForSeconds (intervalBulletsInRound);
			}
		}
	}
}

