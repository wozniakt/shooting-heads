﻿using UnityEngine;
using System.Collections;

public class OppositeAddForce : MonoBehaviour
{	Vector2 direction;
	public float timerForAddForce;
	Rigidbody2D rigid2DHead;
	public bool isAddingForce;
	Transform crosshair;
	Rotate rotate;
	Move move;
	float tmpSpeedX, tmpSpeedY;
	Vector3 tmpVelocity;
	public float recoilForce;
	public bool stopAfterRecoil;
	// Use this for initialization
	void Start ()
	{
		rigid2DHead = GetComponent<Rigidbody2D> ();
		crosshair = transform.Find ("Crosshair");
		rotate = GetComponent<Rotate> ();
		move = GetComponent<Move> ();
	}

	void Update ()
	{
		if (isAddingForce==true && timerForAddForce<0.1f) {
			direction = (new Vector2(this.crosshair.position.x, this.crosshair.position.y))-(Vector2)this.transform.position;
			direction.Normalize();
			float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
			rigid2DHead.velocity = -direction * recoilForce;
			timerForAddForce = timerForAddForce + Time.deltaTime;
			rotate.enabled = false;
		}
		if (isAddingForce == true && timerForAddForce > 0.1f) {
			isAddingForce = false;
			timerForAddForce = 0;
			rotate.enabled = true;
			if (stopAfterRecoil) {
				rigid2DHead.velocity = Vector2.zero;
			}
		}
	}

	void OnCollisionEnter2D(Collision2D col){
		isAddingForce = false;
		timerForAddForce = 0;
		rotate.enabled = true;
		if (stopAfterRecoil) {
			rigid2DHead.velocity = Vector2.zero;
		}
	}
}

