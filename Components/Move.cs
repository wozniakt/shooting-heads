﻿using UnityEngine;
using System.Collections;
using CnControls;
public class Move : MonoBehaviour
{
	public float speedX, speedY;
	Rigidbody2D rigidB2D;
	// Use this for initialization
	void Start ()
	{
		rigidB2D = GetComponent<Rigidbody2D> ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		rigidB2D.velocity = new Vector2 (speedX,speedY);
	}
}

