﻿public enum CustomAttrType
{
	Info, Title

}
[System.AttributeUsage(System.AttributeTargets.Field |  
	System.AttributeTargets.Struct)  
]  
public class CustomAttribite : System.Attribute  
{  
	public string name;  
	public CustomAttrType customAttrType;
	public CustomAttribite(){
	}  
	public CustomAttribite(string _name ,CustomAttrType _customAttrType)  
	{  
		this.name = _name;  
		this.customAttrType = _customAttrType;
	}  
} 