﻿using UnityEngine;
using System.Collections;

public class Shooting_controller_HeroHead : MonoBehaviour
{

	Shoot shoot;
	public HeadState currentHeadState;
	OppositeAddForce oppositeAddForce;
	public float cooldownForButtonShot;
	public BulletType bulletType;
	public float roundsCount;
	public float bulletsInRound;
	public float intervalBulletsInRound;
	public float intervalRounds;
	AmmoClip ammoclip;
	void OnEnable(){
		GlobalEventsManager.instance.OnChangeHeadState += this.ChangeHeadState;
	}
	void OnDisable(){
		GlobalEventsManager.instance.OnChangeHeadState -= this.ChangeHeadState;
	}
	void Start ()
	{	oppositeAddForce = GetComponent<OppositeAddForce> ();
		shoot = GetComponent<Shoot> ( );
		ammoclip = GetComponent<AmmoClip> ();
	}
	public void ChangeHeadState(HeadState oHeadState){
		StartCoroutine (ChangeState_Head (oHeadState));
	}

	IEnumerator ChangeState_Head(HeadState oHeadState){
		switch (oHeadState) {
		case HeadState.Shooting:

			if (currentHeadState!=HeadState.Shooting && ammoclip.BulletsCount>0  && ammoclip.isReloading==false) {
				ammoclip.BulletsCount--;
				GlobalEventsManager.instance.TriggerOnShootAmmo (ammoclip.BulletsCount);
				shoot.startShooting (bulletType,  roundsCount,  bulletsInRound,  intervalBulletsInRound,intervalRounds);
				oppositeAddForce.isAddingForce = true;
				currentHeadState = HeadState.Shooting;
				yield return new WaitForSeconds (cooldownForButtonShot);
			currentHeadState = HeadState.Idle;
			}
			if (currentHeadState!=HeadState.Shooting && ammoclip.BulletsCount<=0 && ammoclip.isReloading==false)
			{
				ammoclip.ReloadAmmoClip();	

			}
			break;
		case HeadState.Idle:
			break;
		}
	}




}

