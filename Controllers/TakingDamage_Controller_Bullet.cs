﻿using UnityEngine;
using System.Collections;

public class TakingDamage_Controller_Bullet : MonoBehaviour
{
	GlobalEventsManager globalEventsManager;
	HP hp;
	Death death;
	public int pointsForDeath, pointsForHit;


	void OnEnable(){
		globalEventsManager=GlobalEventsManager.instance;
		hp = GetComponent<HP> ();
		death= GetComponent<Death> ();
	}

	void OnTriggerEnter2D (Collider2D other)
	{
		if (other.GetComponent<Damage> () != null) {
			hp.HealthPoints = hp.HealthPoints - other.GetComponent<Damage> ().DamagePower;

			if (hp.HealthPoints <= 0) {
				StartCoroutine(death._Death()) ;
			}
		}

	}

	void OnCollisionEnter2D (Collision2D col)
	{
		StartCoroutine(death._Death()) ;

	}
}

