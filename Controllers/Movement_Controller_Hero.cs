﻿using UnityEngine;
using System.Collections;
using CnControls;

public class Movement_Controller_Hero : MonoBehaviour
{
	public  string Hero_HorizontalJoystick, Hero_VerticalJoystick;
	Move move;
	Hero oHero;
	Rigidbody2D rigid2DHero;
	float timerForJump;

	void OnEnable(){
		GlobalEventsManager.instance.OnChangeHeroState += this.ChangeHeroState;
	}
	void OnDisable(){
		GlobalEventsManager.instance.OnChangeHeroState -= this.ChangeHeroState;
	}

	void ChangeHeroState(HeroState oHeroState){
		//oHero.State = oHeroState;
		switch (oHeroState) {
		case HeroState.Jump:
			if (oHero.State == HeroState.Grounded) {
				timerForJump = 0;
				oHeroState = HeroState.Jump;
				oHero.State = oHeroState;
				break;
			} 
			break;
		case HeroState.Falling:
			timerForJump = 1;
			oHero.State = oHeroState;
			break;
		case HeroState.Grounded:
			timerForJump = 0;
			oHero.State = oHeroState;
			break;
		}
	}


	void Start ()
	{
		rigid2DHero=this.GetComponent<Rigidbody2D> ();
		oHero = GetComponent<HeroInit> ().oHero;
		move = GetComponent<Move> ();
	}

	void FixedUpdate () //??? może być Fixed? co wrzucić do Update zwykłego
	{
		move.speedX = CnInputManager.GetAxis (Hero_HorizontalJoystick)*oHero.SpeedX_multipler;
		move.speedY = 1*oHero.SpeedY_multipler;

		if (CnInputManager.GetAxis(Hero_VerticalJoystick)>0 && oHero.State != HeroState.Jump) {
		ChangeHeroState (HeroState.Jump);
		}

		if (oHero.State==HeroState.Jump && timerForJump<0.2f) {
			rigid2DHero.AddForce (Vector3.up * 400 );
			timerForJump = timerForJump + Time.deltaTime;
		}
		if (oHero.State==HeroState.Jump && timerForJump>=0.2f) {
			GlobalEventsManager.instance.TriggerChangeHeroStateo (HeroState.Falling);
		}
	}
	void OnTriggerEnter2D(Collider2D  other){
		if ( other.CompareTag("Ground")) {
			GlobalEventsManager.instance.TriggerChangeHeroStateo (HeroState.Grounded);
		}
	}
}

