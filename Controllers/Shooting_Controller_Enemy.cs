﻿using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Shoot))]
public class Shooting_Controller_Enemy : MonoBehaviour
{	
	Shoot shoot;
	public BulletType bulletType;
	public enum ShootingMode {Normal, Crazy};

	public ShootingMode shootingMode;
	string shootingModeName;
	Coroutine shootingCoroutine;


	void Start ()
	{
		//ShootingMode shootingMode;
		shootingModeName = shootingMode.ToString();

		shoot = GetComponent<Shoot> ();
		shootingCoroutine= StartCoroutine (shootingModeName,2f);

	}
	IEnumerator Normal(float delayTimeBeforeFirstBullet){
		yield return new WaitForSeconds (delayTimeBeforeFirstBullet);
		shoot.startShooting (bulletType, 1, 1, 1, 0);
		StopCoroutine (shootingCoroutine);
		shootingCoroutine= StartCoroutine (shootingModeName,2f);
	}
	IEnumerator Crazy(float delayTimeBeforeFirstBullet){
		yield return new WaitForSeconds (delayTimeBeforeFirstBullet);
		shoot.startShooting (bulletType, 1,10, 0.1f, 3);
		StopCoroutine (shootingCoroutine);
		shootingCoroutine= StartCoroutine (shootingModeName,2f);
	}

	IEnumerator Crazy2(float delayTimeBeforeFirstBullet){
		yield return new WaitForSeconds (delayTimeBeforeFirstBullet);
		shoot.startShooting (bulletType, 1,10, 0.1f, 3);
		StopCoroutine (shootingCoroutine);
		shootingCoroutine= StartCoroutine (shootingModeName,2f);
	}



	// Update is called once per frame

}

