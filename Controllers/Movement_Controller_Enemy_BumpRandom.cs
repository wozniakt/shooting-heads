﻿using UnityEngine;
using System.Collections;


[RequireComponent (typeof (Rigidbody2D))]
[RequireComponent (typeof (Rotate))]


public class Movement_Controller_Enemy_BumpRandom : MonoBehaviour

{
	//Move move;
	Rigidbody2D rigidBody2D;
	Rotate rotate;
	Vector2 direction;
	public float speed;
	public Transform crosshair;
	public float speedX, speedY;
	// Use this for initialization
	void Start ()
	{
		rigidBody2D = GetComponent<Rigidbody2D> ();
		rotate = GetComponent<Rotate> ();
		rigidBody2D.AddForce (new Vector2(Random.Range(-speed,speed), Random.Range(-speed,speed))*10);
	}
	



}

