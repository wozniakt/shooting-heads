﻿using UnityEngine;
using System.Collections;

	[RequireComponent (typeof (HP))]
	[RequireComponent (typeof (Death))]

	public class TakingDamage_Controller_Head : MonoBehaviour
	{
		Flickering flickering;
		HP hp;
		Death death;
		GameObject explosion;
		public EffectType effectHit;
		void OnEnable(){
			hp = GetComponent<HP> ();
			death= GetComponent<Death> ();
			flickering = GetComponent<Flickering> ();
		}

	void OnTriggerEnter2D (Collider2D other)
		{
		
			if (other.GetComponent<Damage> () != null) {
				flickering.StartFlickering ();
				explosion=PoolManager.instance.GetPooledObject_Effect (effectHit);
				explosion.transform.position = this.transform.position;
				explosion.SetActive (true);

				hp.HealthPoints = hp.HealthPoints - other.GetComponent<Damage> ().DamagePower;
				GlobalEventsManager.instance.TriggerGetHit (hp.HealthPoints);
				if (hp.HealthPoints <= 0) {
				StartCoroutine(death._Death()) ;

				}
			}

		}
	}

